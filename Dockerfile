FROM alpine:latest
RUN apk update && apk add tcpdump bind-tools tcptraceroute mtr openssl && rm -rf /var/cache/apk/*
